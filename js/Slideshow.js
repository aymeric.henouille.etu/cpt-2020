export class Slideshow {

    constructor(image, element) {
        this.images = image.split(',');
        this.element = element;
        this._index = 0;
        this.show(this.images[this._index]);
    }

    show(image) {
        this.element.html(`<a href="images/${image}.gif">
        <img src="images/${image}.gif" /></a>`);
    }

    next() {
        this._index = this._index + 1 < this.images.length ? this._index + 1 : 0;
        this.show(this.images[this._index]);
    }

    prev() {
        this._index = this._index - 1 > -1 ? this._index - 1 : this.images.length - 1;
        this.show(this.images[this._index]);
    }

    get index() {
        return this._index;
    }

}