import $ from 'jquery'
import { Slideshow } from './Slideshow';

const defaultPage = 'slideshow';

// petit bonus pour un slideshow automatique
const carousel = false;
const carouselDelay = 3000;

$(() => {

    showSection(defaultPage);

    $('nav > ul > li > a').click(element => showSection(element.delegateTarget.id));


    function showSection(sectionName) {
        $('nav > ul > li > a').removeClass('selected');
        $('nav > ul > li > a#' + sectionName).addClass('selected');

        $('body > section').hide();
        $('body > section.' + sectionName + 'Container').show();
    }


    const images = 'twd,got,td';
    const element = $('.slideshowContainer .slideshow');

    const s = new Slideshow(images, element);
    $('.slideshowContainer .prevButton').click(() => s.prev());
    $('.slideshowContainer .nextButton').click(() => s.next());

    if (carousel) {
        setInterval(() => s.next(), carouselDelay);
    }

});